console.log('loading first module');

// Static import
// import {sayHello} from './second.js';
// sayHello();

let myBla;

//Dynamic import
document.getElementById('loader')
  .addEventListener('click', async () => {

    const {sayHello} = await import('./second.js');
     sayHello();

  });

