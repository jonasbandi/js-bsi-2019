'use strict';

let names = ['John', 'Jane', 'Doe'];

// for-in iterates over indexes of an object
for (let name in names) {
    console.log(name);
}

// for-of iterates over elements of an iterable
for (let name of names) {
    console.log(name);
}


// ** Create an iterator object
let fibonacci = {
    [Symbol.iterator]: function() {
        let pre = 0, cur = 1;
        return {
            next() {
                [pre, cur] = [cur, pre + cur]; // using array destructuring to assign two variables -> see later
                return { done: false, value: cur }
            }
        }
    }
};

// ** Or create an iterator as a generator function
function* fibonacci2() { // a generator function
    let [prev, curr] = [0, 1];
    while (true) {
        [prev, curr] = [curr, prev + curr];
        yield curr;
    }
}

console.log('Fibonacci:');
for (var n of fibonacci) {
    // truncate the sequence at 1000
    if (n > 1000)
        break;
    console.log(n);
}


// iterators are based on duck-typing:
//interface IteratorResult {
//    done: boolean;
//    value: any;
//}
//interface Iterator {
//    next(): IteratorResult;
//}
//interface Iterable {
//    [Symbol.iterator](): Iterator
//}
