var arr1 = [1,2,3];
var arr2 = [4,5,6];

// ES5
console.log(arr1);

arr1.push(arr2);
console.log(arr1);


// ES6
var arr1 = [1,2,3];
var arr2 = [4,5,6];

console.log(...arr1);

arr1.push(...arr2);
console.log(arr1);


function addThreeThings(a, b, c) {
    return a + b + c;
}
console.log(addThreeThings(...[1,2,3]));
