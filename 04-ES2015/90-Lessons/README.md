Install the dependencies:

    yarn install

Run the example:

    yarn start

A browser should open at `http://localhost:3000/`