var arr = [1,2,3];
var result1 = [];
var result2 = [];
var result3 = [];

for (var i = 0; i < arr.length; i++){
    var msg = 'Value: ' + arr[i];
    result1.push(function(){ console.log(msg)});
}

$.each(arr, function(idx, item){
    var msg = 'Value: ' + item; // 'this' is also bound to 'item'
    result2.push(function(){ console.log(msg)});
});

arr.forEach(function(item){
    var msg = 'Value: ' + item;
    result3.push(function(){ console.log(msg)});
});

for (var i = 0, lengh = result1.length; i < lengh; i++) {
    result1[i]();
}

for (var i = 0, lengh = result2.length; i < lengh; i++) {
    result2[i]();
}

for (var i = 0, lengh = result3.length; i < lengh; i++) {
    result3[i]();
}