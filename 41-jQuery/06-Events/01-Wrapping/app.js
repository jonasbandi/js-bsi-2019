$(window).on('hashchange', function(event){

    // Some properties are only available on the originalEvent
    console.log('Hashchange: ' + event.originalEvent.newURL)

    console.log('Event is jQuery.Event: ' + (event instanceof jQuery.Event));
});