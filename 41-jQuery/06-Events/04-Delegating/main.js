
var out = $('#log');
var list = $('#list');

$('#list').on('click', 'li.active', function(event){

    log('jqhandle: ' + event.target.firstChild.nodeValue.trim());

});

$('#add').on('click', function () {
    $('<li>')
        .text(new Date())
        .addClass(new Date().getSeconds() % 2 ? 'active' : 'passive')
        .appendTo(list);
});


function log(msg) {
    out.append('<p>' + msg + '</p>');
}