var divs = document.getElementsByTagName('div');

function capture(event) {
    log('capture: ' + this.firstChild.nodeValue.trim());
    //event.stopPropagation();
}

function bubble(event) {
    log('bubble: ' + this.firstChild.nodeValue.trim());
    //event.stopPropagation();
}

function jqhandle(event) {
    log('jqhandle: ' + this.firstChild.nodeValue.trim());
    //event.stopPropagation();
}

for (var i = 0; i < divs.length; i++) {
    divs[i].addEventListener('click', capture, true);
    divs[i].addEventListener('click', bubble, false);
    $(divs[i]).on('click', jqhandle);
}

var $log = $('#log');

function log(msg) {
    $log.append('<p>' + msg + '</p>');
}