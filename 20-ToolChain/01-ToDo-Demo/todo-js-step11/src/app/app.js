import '../css/index.css';
import $ from 'jquery';
import 'fittextjs';
import './base';
import model from './model';


const $todoInput = $('#todo-text');
const $todo = $('#todo-form');

$todoInput.on('blur', updateToDo);
$todo.on('submit', updateToDo);
$todo.on('submit', addToDo);


function updateToDo() {
  model.updateNewToDoModel($todoInput.val());
}

function addToDo() {
  model.addToDo();
}

function renderToDos() {
  console.log('render');
  const $todoList = $('#todo-list');
  $todoList.html('');
  for (const toDo of model.toDoList) {
    $('<li>')
      .appendTo($todoList)
      .text(toDo.title)
      .append(
        $('<button>')
          .text('X')
          .on('click', () => removeToDo(toDo))
      );
  }
}

function renderNewToDo(){
  $todoInput.val(model.newToDo.title);
}

function removeToDo(toDo) {
  console.log('removing');
  model.removeToDo(toDo);
}

$(model).on('modelchange', () => {
  renderNewToDo();
  renderToDos();
});
renderToDos();
