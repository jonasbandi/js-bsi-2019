// simple decorator annotation
function superhero(target){
  target.isSuperhero = true;
  target.prototype.isSupercat = true;
}

@superhero
class Cat {
  constructor(name){}
  meow(){ return 'MEOOOW!'}
}

//console.log('Is superhero? ' + Cat.isSuperhero);

let garfield = new Cat('garfield');
console.log('Supercat:' + garfield.isSupercat);
garfield.meow();