(function(global) {
    'use strict';

    var template = '    <div class="row" id="summary">\n' +
        '        <div class="col-xs-12">\n' +
        '            <h2>Todo-Count: <span id="count"></span></h2>\n' +
        '        </div>\n' +
        '    </div>';

    function SummaryView(){

        var self = this;

        var appElem = $('#app');

        self.render = function (todos) {

            appElem.html(template);

            var summaryElem = $('#count');
            summaryElem.text(todos.length + ' items');
        }
    }


    global.todo = global.todo || {};
    global.todo.SummaryView = SummaryView;

})(window);
