(function(global) {
    'use strict';


    function TodoListView() {

        var self = this;
        var $self = $(self);

        var todoListElem = $('#do');

        self.render = function(todos){
            var todo;

            todoListElem.html('');
            for(var i = 0, len = todos.length; i < len  ; i++ ){
                todo = todos[i];

                var li = $('<li>');
                li.addClass('clearfix');
                li.text(todo.text);

                var span = $('<span>');
                span.addClass('pull-right');

                var button = $('<button>');
                button.addClass('btn btn-xs btn-danger remove glyphicon glyphicon-trash');
                button.on('click', (function(index){
                    return function(){$self.trigger('itemRemoved', index)};
                })(i));

                span.append(button);
                li.append(span);
                todoListElem.append(li);
            }
        }
    }

    global.todo = global.todo || {};
    global.todo.TodoListView = TodoListView;

})(window);
