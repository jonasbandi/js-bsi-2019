(function(global) {
    'use strict';

    var template = '<div class="row" id="main">\n' +
        '        <div class="col-xs-4">\n' +
        '            <h2>Add a ToDo:</h2>\n' +
        '            <form>\n' +
        '                <div class="form-group">\n' +
        '                    <input type="text" id="input"/>\n' +
        '                </div>\n' +
        '                <div class="form-group">\n' +
        '                    <input id="addBtn" type="button" class="btn btn-primary col-xs-4" value="Add"/>\n' +
        '                </div>\n' +
        '            </form>\n' +
        '        </div>\n' +
        '        <div class="col-xs-4">\n' +
        '            <h2>Items: <span id="total"></span></h2>\n' +
        '            <ul id="do" class="list-unstyled "></ul>\n' +
        '        </div>\n' +
        '    </div>';

    function MainView(){

        var self = this;
        var $self = $(self);

        var appElem = $('#app');

        self.render = function (model) {

            appElem.html(template);

            var todoView = new global.todo.TodoView();
            var todoListView = new global.todo.TodoListView();



            $(todoView).on('itemChanged', function (event, text) { $self.trigger(event, text) } );
            $(todoView).on('itemAdded', function (event, text) { $self.trigger(event, text) });
            $(todoListView).on('itemRemoved', function (event, text) { $self.trigger(event, text) } );

            todoView.render(model.getNewTodo());
            todoListView.render(model.getTodoList());
        }
    }


    global.todo = global.todo || {};
    global.todo.MainView = MainView;

})(window);
