describe("TodoList", function() {
  var model;

  beforeEach(function() {
    model = window.todo.createModel();
  });

  it("should update new todo", function() {
    model.updateNewTodo('test');
    expect(model.getNewTodo().text).toEqual('test');
  });

  it("should add a new todo", function() {
    model.addNewTodo();
    expect(model.getTodoList().length).toEqual(1);
  });

    it("should remove a new todo", function() {
    model.addNewTodo();
    model.removeTodoAtIndex(0);
    expect(model.getTodoList().length).toEqual(0);
  });

});
