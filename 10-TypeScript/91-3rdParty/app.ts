

const champions = [
    {name: 'Katniss'},
    {name: 'Peeta'},
    {name: 'Johanna'},
    {name: 'Haymitch'},
    {name: 'Konrad'}
];

const characters  = _.filter(champions, c => c.startsWith('K'));
const out = characters.join(' ');

document.write(out);



// DEMO
// - rename js -> ts
// - add type declaration:
//      declare var _: { filter: (collection: any[], predicate: (e: any) => boolean) => []}
// - npm i -D @types/lodash
