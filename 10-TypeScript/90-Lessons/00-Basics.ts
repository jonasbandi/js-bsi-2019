lesson("About Expectations", () => {

    learn("how to satisfy expectations", function () {
        var expectedTrue = FILL_ME_IN;
        var expectedFalse = FILL_ME_IN;
        expect(expectedTrue).toBeTruthy();
        expect(expectedFalse).toBeFalsy();
    });

    learn("how to expect equality", function () {
        var expectedValue = FILL_ME_IN;
        var actualValue = 1 + 1;

        expect(actualValue === expectedValue).toBeTruthy();
        expect(actualValue == expectedValue).toBeTruthy();
        expect(actualValue).toEqual(expectedValue);
        expect(actualValue).toBe(expectedValue);
    });

});
