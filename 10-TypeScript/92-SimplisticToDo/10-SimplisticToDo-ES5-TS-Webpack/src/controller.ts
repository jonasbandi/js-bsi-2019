'use strict';
import model = require('./model');
import todoView = require('./todoView');
import todoListView = require('./todoListView');

var controller = {

    init: function () {
        todoView.init();
        todoListView.init();

        $(todoView).on('todo-entered', function (event, todoText) {
            updateTodo(todoText)
        });
        $(todoView).on('todo-added', addItem);
        $(todoListView).on('item-removed', function (event, index) {
            removeItemAtIndex(index)
        });

        todoView.render(model.newTodo);
        todoListView.render(model.todos);
    }
};

function updateTodo(todoText) {
    model.newTodo.text = todoText;
    model.newTodo.created = new Date();
}

function addItem() {
    model.todos.push(model.newTodo);
    model.newTodo = {text: '', created: new Date()};
    todoView.render(model.newTodo);
    todoListView.render(model.todos);
}

function removeItemAtIndex(index) {
    model.todos.splice(index, 1);
    todoListView.render(model.todos);
}

export = controller;
