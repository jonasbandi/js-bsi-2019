'use strict';
class Person {
    constructor(private firstName, private lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    fullName(){
        return `${this.firstName} ${this.lastName}`;
    }

    fullNameReversed(){
        return `${this.lastName} ${this.firstName}`;
    }
}

let person = new Person('Tyler', 'Durden');

let button = document.createElement('button');
button.textContent = "Click Me!";
button.onclick = () => alert(person.fullName());

document.body.appendChild(button);
