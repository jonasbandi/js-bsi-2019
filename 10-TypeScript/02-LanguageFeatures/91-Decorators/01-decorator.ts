// Transpile the script with `tsc` (`tsconfig.json`) has to be present
// Run the script with `node build/01-decorator.js`
import 'reflect-metadata';
declare const Reflect: any;


// simple class decorator
function superheroDecorator(target) {
    target.isSuperhero = true;
    target.prototype.superhero = true;
}

@superheroDecorator
class Cat {
    constructor(name: string, age: number) {}

    meow() { return 'MEOOOW!'; }
}

console.log('Is superhero? ' + Cat['isSuperhero']);

let garfield = new Cat('garfield', 43);
console.log('Superhero:' + garfield['superhero']);
console.log(garfield.meow());


//// DEMO: Different types of decorator implementations

// function extendingDecorator<T extends {new(...args:any[]):{}}>(target:T){
//     return class extends target {
//         superhero = true;
//     }
// }

// function extendingDecoratorFactory(type: string) {
//     return function <T extends { new(...args: any[]): {} }>(target: T) {
//         return class extends target {
//             superhero = type;
//         }
//     }
// }

// function loggingDecorator(target: any, propertyKey: string, descriptor: PropertyDescriptor): any {
//     var originalMethod = descriptor.value;
//
//     descriptor.value = function () {
//
//         console.log('Going to call: ' + target.constructor.name + '->' + propertyKey);
//         var result = originalMethod.apply(this, arguments);
//         console.log('Call finished: ' + target.constructor.name + '->' + propertyKey);
//         return result;
//     };
//
//     return descriptor;
// }


//// DEMO: emit metadata
// console.log(Reflect.getMetadata('design:paramtypes', Cat));
// console.log(Reflect.getMetadata('design:paramtypes', Cat.prototype, 'meow'));