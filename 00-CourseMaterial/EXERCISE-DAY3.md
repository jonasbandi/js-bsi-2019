

## Exercise 10: TypeScript from Scratch

Install the TypeScript compiler, write a TypeScript class and inspect the transpiled result:

- Start a new npm project: `npm init`
- Install TypeScript `npm i -D typescript`
- Create the TypeScript configuration: `node_modules/.bin/tsc --init` (or use WebStorm *Create New -> tsconfic.json* and adjust it.
- Create a npm task that transpiles TypeScript to JavaScript with `tsc` 
- Write a ES2015 class and annotate it with type information
- Inspect the transpiled output
- 

## Exercise 11: TypeScript with 3rd Party Libraries

In the example `10-TypeScript/91-3rdParty`:

- Run the example:

		npm install		
		npm start
	
- A browser should open and you see an error at runtime.
- Rename the file `app.js` to `app.ts` ... do you see any changes?
- Install the type definitions for `lodash`:

		npm install @types/lodash
	
- What did change? You should now get a compiler error ...



## Exercise 12: Plain MVC

1. Simple Example:  
   Extend the example `09-SPA/10-MVC/01-Simplest-jQuery` so that a URL-link is displayed on the page below the framework logo picture.
2. ToDo List: 
   Change the ToDo List application from example `09-SPA/10-MVC/02-SimplisticToDo-NoMVC` to use the MVC-pattern. The goal is to extract the state of the application from the DOM into a JavaScript object model.