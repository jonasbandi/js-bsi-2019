



## Exercise 6: jQuery Selectors

Open `41-jQuery/02-Selectors/index.html` in the browser and play with the lab.

Solve the tasks in `41-jQuery/90-jQuery-Selectors`
 - Open `41-jQuery/90-jQuery-Selectors/index.html` in the browser
- Edit `41-jQuery/90-jQuery-Selectors/main.js` according to the comments in the code





## Preparation Async Exercises (API Endpoint)

Run the server providing the API endpoints for the exercises.

In the directory `08-Async/_server`:

```
npm install 
npm start
```

*Note:* `npm install` installs the dependencies. You have to run that only once.

The server can be stopped with `Ctrl-C`.

##### Word-API

This server provides a HTTP-based API from which you can get 9 words that make up a complete sentence. You can get the first word from the following URL:

```
http://localhost:3456/word/0
```

The last parameter can be varied between 0 and 8.



You can use Postman (https://www.getpostman.com/) to play with the API.



## Exercise 7: AJAX with Callbacks

#### 7.1 Word API Client

Inspect the example `08-Async/10-Callbacks/91-Words-jQuery-callbacks`.

Open the web-page `index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.
- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use jQuery with callbacks to implement the logic.



## Exercise 8: AJAX with Promises

#### 8.1 Word API Client

Inspect the example `08-Async/20-Promises/91-Words-promise`.

Important: Do not use Internet Explorer to run this example since it does not implement native promises!

Inspect the web-page `index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.
- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use axios and Promises to implement the logic.

#### 8.2 Word API Client - Async/Await

Rewrite the solution from exercise 1.1 using `async` and `await`.



## Exercise 9: AJAX with Observables

#### 9.1 Word API Client

Inspect the example `08-Async/30-Observables/92-Words-exercise`.

Inspect the web-page `app/index.html`. The logic in the script loads the first word from the server and renders it on the page.

Implement the "waterfall" scenario: All words should be loaded one after the other from the server. The next word should only be queried when the previous word has been received.

Use axios and RxJS for the implementation.

